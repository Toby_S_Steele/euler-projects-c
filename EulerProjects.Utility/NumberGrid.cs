﻿using System;
using System.IO;
using NLog;

namespace EulerProjects.Utility
{
	public sealed class NumberGrid
	{
		public NumberGrid(int xAxisLimit, int yAxisLimit, string numberGridSource)
		{
			s_logger.Debug("Instantiating...");
			m_xAxisLimit = xAxisLimit;
			m_yAxisLimit = yAxisLimit;
			m_grid = PopulateGridArray(numberGridSource);
			m_highestProduct = 0;
			s_logger.Debug("Instantiated. Grid: {0}, HighestProduct: {1}, xAxisLimit: {2}, yAxisLimit: {3}",
				m_grid.ToString(), m_highestProduct, m_xAxisLimit, m_yAxisLimit);
		}

		public int[,] Grid
		{
			get { return m_grid; }
		}
		public int HighestProduct
		{
			get { return m_highestProduct; }
		}
		public int XLimit
		{
			get { return m_xAxisLimit; }
		}
		public int YLimit
		{
			get { return m_yAxisLimit; }
		}

		private int[,] PopulateGridArray(string numberGridSource)
		{
			s_logger.Debug("Called.");
			int[,] numberArray = new int[m_xAxisLimit, m_yAxisLimit];

			s_logger.Debug("Reading {0}.", numberGridSource);
			string[] rows = File.ReadAllLines(numberGridSource);
			Array.Reverse(rows);
			for (int rowEnumerator = 0; rowEnumerator < m_yAxisLimit; rowEnumerator++)
			{
				string row = rows[rowEnumerator];

				string[] columns = row.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
				for (int columnEnumerator = 0; columnEnumerator < m_xAxisLimit; columnEnumerator++)
				{
					string column = columns[columnEnumerator];

					int currentNumber;
					if (!int.TryParse(column, out currentNumber))
						throw new ArgumentException();

					numberArray[rowEnumerator, columnEnumerator] = currentNumber;
				}
			}

			return numberArray;
		}

		int m_highestProduct;
		readonly int m_xAxisLimit;
		readonly int m_yAxisLimit;
		readonly int[,] m_grid;
		static readonly Logger s_logger = LogManager.GetCurrentClassLogger();
	}
}
