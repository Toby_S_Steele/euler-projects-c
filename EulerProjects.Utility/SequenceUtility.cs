﻿using System.Linq;

namespace EulerProjects.Utility
{
	public class SequenceUtility
	{
		public static bool IsPalindrome(ulong testResult)
		{
			return IsPalindrome(testResult.ToString());
		}

		public static bool IsPalindrome(string testString)
		{
			string testStringReversed = new string(testString.ToCharArray().Reverse().ToArray());
			return testString == testStringReversed;
		}
	}
}
