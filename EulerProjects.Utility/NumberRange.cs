﻿using System;
using System.Collections.Generic;
using NLog;

namespace EulerProjects.Utility
{
	public sealed class NumberRange
	{
		public NumberRange(int x, int y, int xDifference, int yDifference, NumberGrid grid)
		{
			s_logger.Debug("###################################################\\r\\nInstantiating...");
			m_xCoordinate = x;
			m_yCoordinate = y;
			m_xDifference = xDifference;
			m_yDifference = yDifference;
			m_grid = grid;
			m_sequence = GetSequence();
			m_product = GetProduct();
			s_logger.Debug("Instantiated. XCoordinate: {0}, YCoordinate: {1}, XDifference: {2}, YDifference: {3}, Product: {4}",
				m_xCoordinate, m_yCoordinate, m_xDifference, m_yDifference, m_product);
		}

		public int Product
		{
			get { return m_product; }
		}

		public List<int> Sequence
		{
			get { return m_sequence; }
		}

		private List<int> GetSequence()
		{
			List<int> sequence = new List<int>();

			Func<int, int, int, int, bool> funcXRangeDeterminer = null;
			Func<int, int, int> funcXIncrementor = null;
			GetIteratorFunctions(m_xDifference, out funcXRangeDeterminer, out funcXIncrementor);

			Func<int, int, int, int, bool> funcYRangeDeterminer = null;
			Func<int, int, int> funcYIncrementor = null;
			GetIteratorFunctions(m_yDifference, out funcYRangeDeterminer, out funcYIncrementor);
			
			for (int i = m_xCoordinate, j = m_yCoordinate;
				IsWithinRange(funcXRangeDeterminer.Invoke(i, m_grid.XLimit, m_xDifference, m_xCoordinate), funcYRangeDeterminer.Invoke(j, m_grid.YLimit,m_yDifference, m_yCoordinate));
				i = funcXIncrementor(i, m_xDifference), j = funcYIncrementor(j, m_yDifference))
			{
				sequence.Add(m_grid.Grid[i, j]);
				s_logger.Debug("Sequence so far: {0}", string.Join(", ", sequence.ToArray()));
			}

			return sequence;
		}

		private bool IsWithinRange(bool xRangeResult, bool yRangeResult)
		{
			if (m_xDifference > 0)
				return xRangeResult && yRangeResult;
			else // m_yDifference > 0
				return yRangeResult ^ xRangeResult;
		}

		private void GetIteratorFunctions(int difference,
			out Func<int, int, int, int, bool> funcRangeDeterminer, out Func<int, int, int> funcIncrementor)
		{
			funcIncrementor = null;
			funcRangeDeterminer = null;

			if (difference >= 0)
			{
				funcRangeDeterminer = DeterminePositiveRange;
				funcIncrementor = IncrementIfDifferenceHasValue;
			}
			else
			{
				funcRangeDeterminer = DetermineNegativeRange;
				funcIncrementor = DecrementIfDifferenceHasValue;
			}
		}

		private int GetProduct()
		{
			s_logger.Debug("Called.");
			s_logger.Debug("Numbers in sequence: {0}", string.Join(",", m_sequence.ToArray()));
			int product = 1;
			if (m_sequence.Count == 4)
			{
				s_logger.Debug("Finding product of range {0} + ({2}), {1} + ({3}).", m_xCoordinate, m_yCoordinate, m_xDifference, m_yDifference);
				product = 1;
				foreach(int i in m_sequence)
					product = product * i;
				s_logger.Debug("Final product of range {0} + ({2}), {1} + ({3}): {4}.", m_xCoordinate, m_yCoordinate, m_xDifference, m_yDifference, product);
			}
			else
			{
				s_logger.Error("SequenceCount: {0} Sequence: {1}", m_sequence.Count, string.Join(", ", m_sequence.ToArray()));
			}
			return product;
		}

		private static bool DeterminePositiveRange(int i, int axisLimit, int difference, int coordinate)
		{
			return i < axisLimit && (i == 0 || (i > 0 && i < difference + coordinate));
		}

		private static bool DetermineNegativeRange(int i, int axisLimit, int difference, int coordinate)
		{
			return i < axisLimit && (i == 0 || (i > 0 && i < coordinate - difference));
		}

		private static int IncrementIfDifferenceHasValue(int i, int difference)
		{
			return difference > 0 ? i += 1 : i;
		}

		private static int DecrementIfDifferenceHasValue(int i, int difference)
		{
			return difference < 0 ? i -= 1 : i;
		}

		readonly int m_xCoordinate;
		readonly int m_yCoordinate;
		readonly int m_xDifference;
		readonly int m_yDifference;
		readonly int m_product;
		readonly List<int> m_sequence;
		readonly NumberGrid m_grid;
		static readonly Logger s_logger = LogManager.GetCurrentClassLogger();
	}
}
