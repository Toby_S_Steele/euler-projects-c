﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace EulerProjects.Utility
{
	public static class NumberUtility
	{
		public static bool AreAllFactorsFound(Collection<ulong> primeFactors, ulong testNumber)
		{
			bool bAllFactorsFound = false;
			if (primeFactors.Count > 1)
			{
				ulong nProduct = 1;
				foreach (ulong nFactor in primeFactors)
					nProduct *= nFactor;

				if (nProduct >= testNumber)
					bAllFactorsFound = true;
			}
			return bAllFactorsFound;
		}

		public static int[] FindAllWholeDivisors(int number)
		{
			List<int> divisors = new List<int>();
			divisors.Add(1);

			if (number != 1)
				divisors.Add(number);

			for (int i = 2; i < number; i++)
			{
				if (number % i == 0)
				{
					int quotient = number / i;
					if (divisors.Contains(i) || divisors.Contains(quotient))
						break;
					divisors.Add(i);
					divisors.Add(quotient);
				}
			}

			divisors.Sort();
			return divisors.ToArray();
		}

		public static int[] GeneratePrimeNumbers(int limit)
		{
			List<int> primes = Enumerable.Range(2, limit).ToList();

			// apply Eratosthenes' Sieve (thanks, Wikipedia)
			for (int i = 3; i < limit; i++)
			{
				primes.RemoveAll(x => x != i && x % i == 0);
			}

			return primes.ToArray();
		}

		public static bool IsEven(int number)
		{
			return number % 2 == 0;
		}

		public static bool IsPrime(ulong testNumber)
		{
			ulong i = 2;
			if (testNumber < i)
				return false;

			double limit = Math.Sqrt((double)testNumber);
			while (i <= limit)
			{
				if (testNumber % i == 0)
					return false;
				i++;
			}
			return true;
		}
	}
}
