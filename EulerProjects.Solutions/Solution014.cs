﻿using System.Collections.Generic;

namespace EulerProjects.Solutions
{
	public sealed class Solution014
	{
		public static object FindLongestCollatzSequenceUnderOneMillion()
		{
			int highScore = 0;
			for (int i = c_startingNumberLimit; i > 0; i--)
			{
				long currentNum = i;
				List<long> collatzSequence = new List<long>() { currentNum };

				while (currentNum != 1)
				{
					currentNum = GetNextNumberInSequence(currentNum);
					collatzSequence.Add(currentNum);
				}

				if (collatzSequence.Count >= highScore)
					highScore = collatzSequence.Count;
			}
			return highScore;
		}

		private static long GetNextNumberInSequence(long currentNum)
		{
			if (currentNum % 2 == 0)
				return currentNum / 2;
			else
				return 3 * currentNum + 1;
		}

		const int c_startingNumberLimit = 1000000;
	}
}
