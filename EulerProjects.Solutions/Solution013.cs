﻿using System.IO;
using System.Linq;

namespace EulerProjects.Solutions
{
	public sealed class Solution013
	{
		public static object First10NumbersOf100FiftyDigitNumbers()
		{
			//mcFreakin' rangin-frangin ... doh!!!!!! (this was too simple)
			string[] lines = File.ReadAllLines(@".\Data\EulerProject13_FiftyDigitNumbers.txt");
			double[] nums = lines.Select(x => double.Parse(x)).ToArray();
			double total = 0.0;
			for (int i = 0; i < lines.Length; i++)
			{
				total += nums[i];
			}

			return total;
		}
	}
}
