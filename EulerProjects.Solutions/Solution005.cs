﻿using System;
using System.Collections.ObjectModel;

namespace EulerProjects.Solutions
{
	public sealed class Solution005
	{
		public static object GetSmallestNumberEvenlyDivisibleByNumbers1To20()
		{
			bool smallestNumberFound = false;
			ulong possibleDivisor = c_lowestPossibleDivisor;
			while (!smallestNumberFound)
			{
				Collection<uint> numbersFound = new Collection<uint>();
				for (uint i = 20; i > 0; i--)
				{
					if (possibleDivisor % i == 0)
						numbersFound.Add(i);
					else
						break;
				}

				if (numbersFound.Count == 20)
					smallestNumberFound = true;
				else
					possibleDivisor += 20;
			}

			return possibleDivisor;
		}

		private const ulong c_lowestPossibleDivisor = 400;
	}
}
