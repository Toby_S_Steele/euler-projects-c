﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EulerProjects.Utility;

namespace EulerProjects.Solutions
{
	public sealed class Solution010
	{
		public static object CalculateSummationOfPrimesBelowTwoMillion()
		{
			long primeSum = 0;
			for (long i = 2; i <= 2000000; i++)
			{
				if (NumberUtility.IsPrime((ulong)i))
					primeSum += i;
			}

			return primeSum;
		}
	}
}
