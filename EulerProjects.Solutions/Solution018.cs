﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace EulerProjects.Solutions
{
	public sealed class Solution018
	{
		public static object FindMaximumTotalOfTopToBottomOfTriangle()
		{
			string [] lines = File.ReadAllLines(dataPath);
			int[][] numbers = lines
				.Select(l => l.Split(new string[]{" "}, StringSplitOptions.None)
					.Select(n => int.Parse(n)).ToArray()).ToArray();

			int total = 0;
			for(int i = 0, j = 0; i < numbers.Length; i++)
			{
				total += numbers[i][j];
				if (i + 1 < numbers.Length)
				{
					int compare = numbers[i + 1][j].CompareTo(numbers[i + 1][j + 1]);
					j = compare >= 0 ? j : j + 1;
				}
			}

			return total;
		}

		const string dataPath = @".\Data\EulerProject18_MaximumTotalOfTriangle.txt";
	}
}
