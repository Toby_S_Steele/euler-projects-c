﻿using System;
using NLog;

namespace EulerProjects.Solutions
{
	public class ProjectEulerSolution
	{
		public ProjectEulerSolution(Func<object> solution, Action<object, int> displayOutput)
		{
			m_solution = solution;
			m_displayOutput = displayOutput;
		}
		
		public ProjectEulerSolution(Func<object> solution)
			: this (solution, DisplayOutput)
		{ }

		public object FinalResult
		{
			get { return m_finalResult; }
		}
		
		public void RunSolutionAndDisplayOutput(int solutionNumber)
		{
			m_finalResult = m_solution.Invoke();
			m_displayOutput.Invoke(m_finalResult, solutionNumber);
		}

		internal static void DisplayOutput(object finalResult, int solutionNumber)
		{
			Console.WriteLine("Solution {0}: {1}", solutionNumber, finalResult);
		}

		protected Func<object> m_solution;
		protected Action<object, int> m_displayOutput;
		protected object m_finalResult;
		protected static readonly Logger s_logger = LogManager.GetCurrentClassLogger();
	}
}
