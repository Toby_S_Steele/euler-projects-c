﻿using System.Linq;
using System.Numerics;

namespace EulerProjects.Solutions
{
	public sealed class Solution016
	{
		public static object SumDigitsOfTwoToThePowerOf1000()
		{ 
			BigInteger number = 2;
			BigInteger result = BigInteger.Pow(number, c_power);
			long[] arrayOfNumbers = result.ToString().Select(x => long.Parse(x.ToString())).ToArray();

			long total = 0;
			foreach(long i in arrayOfNumbers)
				total += i;

			return total;
		}

		const int c_power = 1000;
	}
}
