﻿using EulerProjects.Utility;

namespace EulerProjects.Solutions
{
	public sealed class Solution012
	{
		public static object FindFirstTriangleNumberWithOver500Divisors()
		{
			int i = 2;
			int term = 1;
			while (GetDivisors(term).Length < 500)
			{
				term += i;
				i++;
			}

			return term;
		}

		private static int[] GetDivisors(int i)
		{
			return NumberUtility.FindAllWholeDivisors(i);
		}
	}
}
