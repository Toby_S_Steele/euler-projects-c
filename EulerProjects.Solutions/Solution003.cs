﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using EulerProjects.Utility;

namespace EulerProjects.Solutions
{
	public sealed class Solution003
	{
		public static ulong[] GetPrimeFactorsOfRidiculousHugeNumber()
		{
			Collection<ulong> primeFactors = new Collection<ulong>();

			ulong i;
			for (i = 2; i < c_ridiculouslyHugeNumber; i++)
			{
				if (c_ridiculouslyHugeNumber % i == 0 && NumberUtility.IsPrime(i))
				{
					primeFactors.Add(i);
					if (NumberUtility.AreAllFactorsFound(primeFactors, c_ridiculouslyHugeNumber))
						break;
				}
			}

			if (i < c_ridiculouslyHugeNumber)
				return primeFactors.ToArray();
			else
				return null;
		}

		public static void DisplayLargestPrimeFactor(object finalResult, int solutionNumber)
		{
			object result = ((ulong[])finalResult).Last();
			ProjectEulerSolution.DisplayOutput(result, solutionNumber);
		}

		private const ulong c_ridiculouslyHugeNumber = 600851475143;
	}
}
