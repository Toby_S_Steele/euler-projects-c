﻿using System.Collections.Generic;
using System.Linq;
using EulerProjects.Utility;

namespace EulerProjects.Solutions
{
	public sealed class Solution002
	{
		public static object SumEvenFibonacciNumbersTo4000000()
		{
			List<int> fibonacciSequence = new List<int>() { 1, 2 };
			int sum = 2;
			while (fibonacciSequence.Last() < c_limit)
			{
				int lastFibonacciIndex = fibonacciSequence.Count - 1;
				int nextNumber = fibonacciSequence[lastFibonacciIndex] + fibonacciSequence[lastFibonacciIndex - 1];
				fibonacciSequence.Add(nextNumber);

				if (NumberUtility.IsEven(nextNumber))
					sum += nextNumber;
			}

			return sum;
		}

		const int c_limit = 4000000;
	}
}
