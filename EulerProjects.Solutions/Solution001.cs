﻿namespace EulerProjects.Solutions
{
	public sealed class Solution001
	{
		public static object CalculateMultiplesOf3And5To1000()
		{
			int multipleSum = 0;
			for (int i = 3; i < c_limit; i++)
			{
				if (i % 3 == 0 || i % 5 == 0)
					multipleSum += i;
			}
			return multipleSum;
		}

		const int c_limit = 1000;
	}
}
