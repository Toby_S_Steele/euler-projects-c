﻿using System;

namespace EulerProjects.Solutions
{
	public sealed class Solution006
	{
		public static object GetDifferenceOfSumOfSquaresAndSquaresOfSum100()
		{
			ulong sumOfSquaresOf100 = GetSumOfSqaresOf100();
			ulong squaresOfSumOf100 = GetSquaresOfSumOf100();

			if (sumOfSquaresOf100 > squaresOfSumOf100)
				return sumOfSquaresOf100 - squaresOfSumOf100;
			else
				return squaresOfSumOf100 - sumOfSquaresOf100;
		}

		private static ulong GetSumOfSqaresOf100()
		{
			ulong sum = 0;
			for (uint i = 1; i <= c_total; i++)
			{
				ulong square = i * i;
				sum += square;
			}
			return sum;
		}

		private static ulong GetSquaresOfSumOf100()
		{
			ulong sum = 0;
			for (uint i = 1; i <= c_total; i++)
			{
				sum += i;
			}

			return sum * sum;
		}

		private const uint c_total = 100;
	}
}
