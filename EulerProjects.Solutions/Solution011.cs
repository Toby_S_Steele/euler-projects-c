﻿using System.Collections.Generic;
using EulerProjects.Utility;

namespace EulerProjects.Solutions
{
	public sealed class Solution011
	{
		public static object FindLargestProductIn20x20Grid()
		{
			s_numberGrid = new NumberGrid(c_axisLimit, c_axisLimit, c_numberGridFile);
			s_numberRanges = new List<NumberRange>();
			int finalResult = 0;

			for (int y = 0; y < s_numberGrid.YLimit; y++)
			{
				for (int x = 0; x < s_numberGrid.XLimit; x++)
				{
					AddNumberRangeIfValid(x, y, 4, 0);
					AddNumberRangeIfValid(x, y, 4, 4);
					AddNumberRangeIfValid(x, y, 0, 4);
					AddNumberRangeIfValid(x, y, -4, 4);
					AddNumberRangeIfValid(x, y, -4, 0);
					AddNumberRangeIfValid(x, y, -4, -4);
					AddNumberRangeIfValid(x, y, 0, -4);
					AddNumberRangeIfValid(x, y, 4, -4);
				}
			}

			foreach (NumberRange range in s_numberRanges)
				finalResult = range.Product > (int)finalResult ? range.Product : finalResult;

			return finalResult;
		}

		private static void AddNumberRangeIfValid(int x, int y, int xDifference, int yDifference)
		{
			int newXCoordinate = x + xDifference;
			int newYCoordinate = y + yDifference;

			NumberRange numberRange = new NumberRange(x, y, xDifference, yDifference, s_numberGrid);
			
			if (numberRange.Sequence.Count == 4)
				s_numberRanges.Add(numberRange);
			//else
				//s_logger.Warn("Range [{0} + ({1}) = {2}], [{3} + ({4}) = {5}] did not produce the required set of numbers for a sequence. \\r\\n",
				//	x, xDifference, newXCoordinate, y, yDifference, newYCoordinate);
		}

		static NumberGrid s_numberGrid;
		static List<NumberRange> s_numberRanges;
		const int c_axisLimit = 20;
		const string c_numberGridFile = @".\Data\EulerProject11_NumberGrid.txt";
	}
}
