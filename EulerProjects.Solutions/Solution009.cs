﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EulerProjects.Solutions
{
	public sealed class Solution009
	{
		public static object FindSpecialPythagoreanTriplet()
		{
			int result = 0;
			for (int c = 500; c > 3; c--)
			{
				for (int b = (c - 1); b > 2; b--)
				{
					for (int a = (b - 1); a > 1; a--)
					{
						if (Math.Pow(a, 2) + Math.Pow(b, 2) == Math.Pow(c, 2) && a + b + c == 1000)
							result = a * b * c;
					}
				}
			}
			return result;
		}

		private const int c_pythagoreanTotal = 1000;
	}
}
