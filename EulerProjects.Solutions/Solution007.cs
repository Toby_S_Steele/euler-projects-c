﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using EulerProjects.Utility;

namespace EulerProjects.Solutions
{
	public static class Solution007
	{
		public static object Find10001stPrimeNumber()
		{
			Collection<ulong> primeNumbers = new Collection<ulong>() { c_firstPrime };
			ulong testNumber = c_firstPrime + 1;

			while (primeNumbers.Count < c_totalPrimeNumbers)
			{
				if (NumberUtility.IsPrime(testNumber))
					primeNumbers.Add(testNumber);

				testNumber += 2;
			}

			return primeNumbers.Last();
		}

		private const int c_firstPrime = 2;
		private const int c_totalPrimeNumbers = 10001;
	}
}
