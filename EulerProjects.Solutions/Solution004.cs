﻿using System.Collections.ObjectModel;
using System.Linq;
using EulerProjects.Utility;

namespace EulerProjects.Solutions
{
	public sealed class Solution004
	{
		public static object GetLargestPalindromeOfTwoThreeDigitNumbers()
		{
			Collection<ulong> palindromes = new Collection<ulong>();

			for (uint multiplicant = 100; multiplicant < 1000; multiplicant++)
			{
				for (uint multiplier = multiplicant; multiplier < 1000; multiplier++)
				{
					ulong testResult = multiplicant * multiplier;

					if (SequenceUtility.IsPalindrome(testResult))
						palindromes.Add(testResult);
				}
			}

			return palindromes.OrderByDescending(x => x).First();
		}
	}
}
