﻿using System.Collections;
using System.Linq;

namespace EulerProjects.Solutions
{
	public static class Solution015
	{
		public static object FindAllLatticeRoutesIn20x20Grid()
		{
			// populate grid
			for (int i = 0; i < c_gridLimit; i++)
			{
				for (int j = 0; j < c_gridLimit; j++)
				{
					s_grid[i, j] = new LatticePoint(i, j);

					// get previous and next
					if (i != 0)
					{
						if (i < c_gridLimit - 1)
							s_grid[i - 1, j].Next[0] = s_grid[i, j];
						s_grid[i, j].Previous[0] = s_grid[i - 1, j];
					}

					if (j != 0)
					{
						if (j < c_gridLimit - 1)
							s_grid[i, j - 1].Next[1] = s_grid[i, j];
						s_grid[i, j].Previous[1] = s_grid[i, j - 1];
					}
				}
			}

			int pointsHit = 0;
			int pathsFound = 0;
			int timesThrough = 0;
			while (pointsHit < s_grid.Length && timesThrough < c_gridLimit)
			{
				LatticePoint current = s_grid[timesThrough,0];
				while (current != null && current.Next.Any(n => n != null && !n.Hit))
				{
					current.Hit = true;
					current = current.Next.First(n => n != null && !n.Hit);
				}
				pathsFound++;
				timesThrough++;
			}

			return "Currently Unsolved";
		}

		const int c_gridLimit = 21;
		static readonly LatticePoint[,] s_grid = new LatticePoint[c_gridLimit, c_gridLimit];
	}

	internal class LatticePoint
	{
		internal LatticePoint(int x, int y, bool hit = false)
		{
			X = x;
			Y = y;
			Hit = hit;
		}

		internal int X { private set; get; }
		internal int Y { private set; get; }
		internal bool Hit { set; get; }

		internal LatticePoint[] Next
		{
			get { return m_next; }
		}
		internal LatticePoint[] Previous
		{
			get { return m_previous; }
		}

		private LatticePoint[] m_next = new LatticePoint[2];
		private LatticePoint[] m_previous = new LatticePoint[2];
	}
}

