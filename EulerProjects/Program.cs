﻿using System;
using System.Collections.Generic;
using EulerProjects.Solutions;

namespace EulerProjects
{
	class Program
	{
		static void Main(string[] args)
		{
			List<ProjectEulerSolution> projectSolutions = new List<ProjectEulerSolution>();
			projectSolutions.Add(new ProjectEulerSolution(Solution001.CalculateMultiplesOf3And5To1000));
			projectSolutions.Add(new ProjectEulerSolution(Solution002.SumEvenFibonacciNumbersTo4000000));
			projectSolutions.Add(new ProjectEulerSolution(Solution003.GetPrimeFactorsOfRidiculousHugeNumber, Solution003.DisplayLargestPrimeFactor));
			projectSolutions.Add(new ProjectEulerSolution(Solution004.GetLargestPalindromeOfTwoThreeDigitNumbers));
			projectSolutions.Add(new ProjectEulerSolution(Solution005.GetSmallestNumberEvenlyDivisibleByNumbers1To20));
			projectSolutions.Add(new ProjectEulerSolution(Solution006.GetDifferenceOfSumOfSquaresAndSquaresOfSum100));
			projectSolutions.Add(new ProjectEulerSolution(Solution007.Find10001stPrimeNumber));
			projectSolutions.Add(new ProjectEulerSolution(Solution008.DetermineGreatestProductOfFiveConsecutiveDigits));
			projectSolutions.Add(new ProjectEulerSolution(Solution009.FindSpecialPythagoreanTriplet));
			projectSolutions.Add(new ProjectEulerSolution(Solution010.CalculateSummationOfPrimesBelowTwoMillion));
			projectSolutions.Add(new ProjectEulerSolution(Solution011.FindLargestProductIn20x20Grid));
			projectSolutions.Add(new ProjectEulerSolution(Solution012.FindFirstTriangleNumberWithOver500Divisors));
			projectSolutions.Add(new ProjectEulerSolution(Solution013.First10NumbersOf100FiftyDigitNumbers));
			projectSolutions.Add(new ProjectEulerSolution(Solution014.FindLongestCollatzSequenceUnderOneMillion));
			projectSolutions.Add(new ProjectEulerSolution(Solution015.FindAllLatticeRoutesIn20x20Grid));
			projectSolutions.Add(new ProjectEulerSolution(Solution016.SumDigitsOfTwoToThePowerOf1000));
			projectSolutions.Add(new ProjectEulerSolution(Solution017.RunSolution));
			projectSolutions.Add(new ProjectEulerSolution(Solution018.FindMaximumTotalOfTopToBottomOfTriangle));

			Console.WriteLine("What solution number are you looking for?");
			string input = Console.ReadLine();

			int i;
			if (!int.TryParse(input, out i))
				Console.WriteLine("Try a number next time, silly!");
			else if (i > projectSolutions.Count)
				Console.WriteLine("Sorry, current solutions only go up to {0}.", projectSolutions.Count);
			else
				projectSolutions[i - 1].RunSolutionAndDisplayOutput(i);

			Console.ReadLine();
		}
	}
}
